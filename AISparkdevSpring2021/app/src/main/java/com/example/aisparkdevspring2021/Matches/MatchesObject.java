package com.example.aisparkdevspring2021.Matches;

public class MatchesObject {
    private String bio;
    private String userID;
    private String name;

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    private String profileImageUrl;

    public MatchesObject (String userID, String name, String profileImageUrl, String bio){
        this.userID = userID;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.bio = bio;

    }
    public String getUserID(){
        return userID;
    }
    public void setUserID(String userID){
        this.userID = userID;

    }
    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getProfileImageUrl(){
        return profileImageUrl;
    }
    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }
}

