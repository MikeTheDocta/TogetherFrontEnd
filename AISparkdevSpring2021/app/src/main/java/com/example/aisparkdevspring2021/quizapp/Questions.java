package com.example.aisparkdevspring2021.quizapp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Questions {

    private String questionText;
    private String optA;
    private String optB;
    private String optC;


    public Questions(String questionText, String optA, String optB, String optC) {
        this.questionText = questionText;
        this.optA = optA;
        this.optB = optB;
        this.optC = optC;

    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getOptA() {
        return optA;
    }

    public void setOptA(String optA) {
        this.optA = optA;
    }

    public String getOptB() {
        return optB;
    }

    public void setOptB(String optB) {
        this.optB = optB;
    }

    public String getOptC() {
        return optC;
    }

    public void setOptC(String optC) {
        this.optC = optC;
    }



    public void setConcatResponses(String concatResponses) {
        this.concatResponses = concatResponses;
    }


    public String responses[];
    public String concatResponses = "";


    public void concat(String[] x) {
        for (int i = 0; i < x.length; i++) {
            concatResponses = concatResponses + x[i];
        }
    }

}
