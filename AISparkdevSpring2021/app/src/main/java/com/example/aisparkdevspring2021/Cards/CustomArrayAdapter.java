package com.example.aisparkdevspring2021.Cards;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.aisparkdevspring2021.R;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapter extends RecyclerView.Adapter<CustomArrayAdapter.ViewHolder> {

    ArrayList<Card> cards;

    public CustomArrayAdapter(ArrayList<Card> cards)
    {
        this.cards = cards;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getName().setText(cards.get(position).getName());

        Log.d("Holder name", holder.getName().getText().toString());

        switch(cards.get(position).getProfileImageUrl()){
            case "default":
                Glide.with(holder.getImage().getContext()).load(R.mipmap.ic_launcher).centerCrop().into(holder.getImage());

                break;
            default:
                Glide.with(holder.getImage().getContext()).load(cards.get(position).getProfileImageUrl()).centerCrop().into(holder.getImage());

                break;

        }
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, nameBack;
        private ImageView image, imageBack;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);

            image = (ImageView) itemView.findViewById(R.id.image);


        }

        public TextView getName() {
            return name;
        }
        public ImageView getImage() {
            return image;
        }
    }
}