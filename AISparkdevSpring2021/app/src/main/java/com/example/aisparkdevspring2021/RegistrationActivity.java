package com.example.aisparkdevspring2021;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.example.aisparkdevspring2021.quizapp.QuizActivity;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private EditText mEmail, mPassword, mName, mPhone, mGender, mConfirmPassword;
    private RadioGroup mRadioGroup;
    private FirebaseAuth mAuth;
    private StorageReference storageReference;
    private ActivityResultLauncher<Intent> uploaderLauncher;
    private ArrayList<Uri> imageURIs;
    private int EXTERNAL_STORAGE_PERMISSION_CODE = 23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        Button superSecretButton = findViewById(R.id.buttonImageUploader);
        Button mRegister = findViewById(R.id.button_signup);
        mPhone = findViewById(R.id.phone);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mConfirmPassword = findViewById(R.id.confirm_password);
        mName = findViewById(R.id.username);
        mRadioGroup = (RadioGroup) findViewById(R.id.gender_select);
        mAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewAccount();
            }
        });

        superSecretButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageUploaderIntent = new Intent(RegistrationActivity.this, ImageUploader.class);
                startActivity(imageUploaderIntent);
            }
        });

        uploaderLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK){
                    Intent data = result.getData();

                    Data dataFinal = (Data) data.getSerializableExtra("data");
                    imageURIs = data.getParcelableArrayListExtra("imageURIs");

                    //FIXME: Implement Async-Await here!!!!
                    String bio = dataFinal.getBio();
                    int groupNumber = getGroupNumber(bio);
                    getPersonalityType(dataFinal.getConcatQuestions(), dataFinal, groupNumber, bio);


                }

                if(result.getResultCode() == RESULT_CANCELED){
                    finish();
                    return;
                }
                else{
                    int code = result.getResultCode();
                    Toast.makeText(getApplicationContext(), "Error Occurred! " + code, Toast.LENGTH_LONG).show();
                    Log.i("Error_Tag", "Error in the image uploader!!!");
                }
            }
        });

    }

    private void createNewAccount(){
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();
        String phone = mPhone.getText().toString();
        String name = mName.getText().toString();
        int selectID = mRadioGroup.getCheckedRadioButtonId();
        RadioButton radioButton =  (RadioButton) findViewById(selectID);
        String gender = radioButton.getText().toString();
        if(radioButton.getText() == null){
            return;
        }

        String pattern = "\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}";

        if(TextUtils.isEmpty(email))
        {
            Toast.makeText(this, "Enter a valid email...", Toast.LENGTH_SHORT);
        }
        else if(TextUtils.isEmpty(password))
        {
            Toast.makeText(this, "Enter a valid password...", Toast.LENGTH_SHORT);
        }
        else if(TextUtils.isEmpty(name))
        {
            Toast.makeText(this, "Enter a valid name...", Toast.LENGTH_SHORT);
        }
        else if(TextUtils.isEmpty(phone) || !phone.matches(pattern))
        {
            Toast.makeText(this, "Enter a valid phone number...", Toast.LENGTH_SHORT);
        }
        else if(!password.equals(confirmPassword))
        {
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT);
        }

        else{
            Data d = new Data();
            d.setEmail(email);
            d.setName(name);
            d.setPassword(password);
            d.setPhone(phone);
            d.setGender(gender);
            d.setPreferredGender( gender.equals("Male")? "Female" : "Male" );


            Intent uploaderIntent = new Intent(RegistrationActivity.this, ImageUploader.class);
            uploaderIntent.putExtra("data", d);
            uploaderLauncher.launch(uploaderIntent);
        }
        return;

    }

    //Figure out how to get the called activities to put out data!!!



    private void registerUser(Data d, String personality, int groupNumber, ArrayList<Uri> imageURIs, String[] gpt2Responses) {

        mAuth.createUserWithEmailAndPassword(d.getEmail(), d.getPassword())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {

                            //Upload normal user info...
                            String userID = FirebaseAuth.getInstance().getUid();
                            DatabaseReference userDB = FirebaseDatabase.getInstance().getReference().child("Users");
                            DatabaseReference user = userDB.child(userID);
                            user.child("Name").setValue(d.getName());
                            user.child("Phone").setValue(d.getPhone());
                            user.child("Personality").setValue(personality);
                            user.child("Primary Group Number").setValue(groupNumber);
                            user.child("Biography").setValue(d.getBio());
                            user.child("Preferred Gender").setValue(d.getPreferredGender());
                            user.child("Gender").setValue(d.getGender());
                            user.child("profileImageUrl").setValue(d.getProfileImageUrl());

                            user.child("Responses").child("0").setValue(gpt2Responses[0]);
                            user.child("Responses").child("1").setValue(gpt2Responses[1]);
                            user.child("Responses").child("2").setValue(gpt2Responses[2]);

                            //Upload images to firebase...
                            final String userKey = mAuth.getCurrentUser().getUid();
                            StorageReference firePic = storageReference.child("profileImages/" + userKey);

                            int numberOfImages = imageURIs.size();


                            for(int imagePos= 0; imagePos < numberOfImages; imagePos++)
                            {


                                firePic.child(Integer.toString(imagePos)).putFile(imageURIs.get(imagePos))
                                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                Snackbar.make(findViewById(android.R.id.content), "Image Upload.", Snackbar.LENGTH_LONG).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                Toast.makeText(getApplicationContext(), "Failed To Upload. ", Toast.LENGTH_LONG).show();// Handle unsuccessful uploads
                                                // ...
                                            }
                                        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                                        //double progressPercent = (100.00 * taskSnapt.getBytesTransferred() / t)
                                    }

                                });
                            }



                            Intent mainIntent = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
                            return;



                    } else{
                            Log.d("Firebase Error", task.getException().toString());
                        }
                }
        });
    }

    private String getPersonalityType(String concatQuestions, Data d, int groupNumber, String bio) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://10.0.0.182:5000/predict_personalities?text=" + concatQuestions;
        String[] ret = {""};

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ret[0] = response.getString("results");
                    String[] gpt2Responses = getGpt2Responses(d, ret[0], imageURIs, groupNumber, bio);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error.getMessage());
            }
        });
        jor.setRetryPolicy(new DefaultRetryPolicy(20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(jor);

        return ret[0];
    }

    private int getGroupNumber(String bio) {
    //Convert to JSONRequest
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://10.0.0.182:5000/predict_hobbies?text=" + bio;
        int[] ret = {0};
        // Request a string response from the provided URL.
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ret[0] = response.getInt("cluster");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error.getMessage());
            }
        });

// Add the request to the RequestQueue.
        queue.add(jor);
        return ret[0];
    }

    private String[] getGpt2Responses(Data dataFinal, String personality, ArrayList<Uri> imageURIs, int groupNumber, String bio){
        final int TOP_SENTENCE_NUMBER = 3;
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://75.33.221.36:3387?bio=" + bio;
        String[] ret = new String[TOP_SENTENCE_NUMBER];
        // Request a string response from the provided URL.
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jArray = response.getJSONArray("sentences");
                    for(int i = 0; i < TOP_SENTENCE_NUMBER; i++){
                        ret[i] = jArray.getString(i);
                    }

                    registerUser(dataFinal, personality, groupNumber, imageURIs, ret);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error.getMessage());
            }
        });

        jor.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(jor);
        return ret;
    }

}