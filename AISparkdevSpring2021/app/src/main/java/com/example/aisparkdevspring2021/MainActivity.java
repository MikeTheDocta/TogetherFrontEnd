package com.example.aisparkdevspring2021;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.aisparkdevspring2021.Cards.Card;
import com.example.aisparkdevspring2021.Cards.CustomArrayAdapter;
import com.example.aisparkdevspring2021.Matches.MatchesActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import static com.example.aisparkdevspring2021.BaseApp.CHANNEL_1_ID;

public class MainActivity extends Activity {


    private CustomArrayAdapter arrayAdapter;
    private int i;

    private FirebaseAuth mAuth;
    private String currentUId;
    private String preferredMatchSex;
    private DatabaseReference userDb, matchDb;
    private NotificationManagerCompat nManager;
    private FirebaseStorage mStorage;
    private StorageReference storageReference;

    ListView listView;
    ArrayList<Card> rowItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        userDb = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
        currentUId = mAuth.getCurrentUser().getUid();
        nManager = NotificationManagerCompat.from(this);
        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        userDb.child(currentUId).child("Preferred Gender").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                preferredMatchSex = snapshot.getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        Button signOut = findViewById(R.id.button_sign_out);
        Button matches = findViewById(R.id.matches_button);
        Button settings = findViewById(R.id.settings_button);


        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MatchesActivity.class);
                startActivity(intent);
                return;
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return;
            }
        });
        rowItems = new ArrayList<Card>();
        arrayAdapter = new CustomArrayAdapter(rowItems);


        RecyclerView matchesList = (RecyclerView)findViewById(R.id.card_layout);
        matchesList.setAdapter(arrayAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2, LinearLayoutManager.HORIZONTAL,false);

        matchesList.setLayoutManager(gridLayoutManager);

        matchDb = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUId).child("connections").child("matches");

        getPreferredUserGender();

        Log.d("rowItems", rowItems.toString());


    }

    private void isConnectionMatch(String userId) {
        DatabaseReference currentUserConnectionsDb = userDb.child(currentUId).child("connections").child("yep").child(userId);
        currentUserConnectionsDb.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Toast.makeText(MainActivity.this, "new Connection", Toast.LENGTH_LONG).show();

                    String key = FirebaseDatabase.getInstance().getReference().child("Chat").push().getKey();

                    userDb.child(snapshot.getKey()).child("connections").child("matches").child(currentUId).child("ChatId").setValue(key);
                    userDb.child(currentUId).child("connections").child("matches").child(snapshot.getKey()).child("ChatId").setValue(key);
                    matchDb.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                sendOnChannel1(findViewById(android.R.id.content).getRootView());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    /*public void checkUserSex(){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference usersDb = userDb.child(user.getUid());
        usersDb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()) {
                    if (snapshot.child("sex").getValue() != null) {
                        userSex = snapshot.child("sex").getValue().toString();
                        switch (userSex)
                        {
                            case "Male":
                                potentialMatchSex = "Female";
                                break;
                            case "Female":
                                potentialMatchSex = "Male";
                                break;
                        }
                        getOppositeSexUsers();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }*/

    public void getPreferredUserGender(){
        userDb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {


                    Iterable<DataSnapshot> children = snapshot.getChildren();

                    for (DataSnapshot child : children) {
                        String childGender = child.child("Gender").getValue().toString();
                        String imageDir = child.getKey();

                        Log.d("Preferred Genders match", Boolean.toString(childGender.equals(preferredMatchSex)));
                        Log.d("Child is not in nope", Boolean.toString(!child.child("connections").child("nope").hasChild(currentUId)));
                        Log.d("Child is not in yep", Boolean.toString(!child.child("connections").child("yep").hasChild(currentUId)));

                        if (childGender.equals(preferredMatchSex) && !child.child("connections").child("nope").hasChild(currentUId) && !child.child("connections").child("yep").hasChild(currentUId)) {

                            String profileImageUrl = "default";
                            if (!child.child("profileImageUrl").getValue().toString().equals("default")) {
                                profileImageUrl = child.child("profileImageUrl").getValue().toString();
                            }

                            Log.d("userImageDir", imageDir);



                            storageReference.child("profileImages").child(imageDir).list(1).addOnSuccessListener(new OnSuccessListener<ListResult>() {
                                @Override
                                public void onSuccess(ListResult listResult) {
                                    for (StorageReference imageRef : listResult.getItems()) {
                                        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                String imageURL = uri.toString();
                                                Card Item = new Card(child.getKey(), child.child("Name").getValue().toString(), imageURL, child.child("Biography").getValue().toString(), 7);
                                                rowItems.add(Item);
                                                arrayAdapter.notifyItemInserted(rowItems.size()-1);
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                // Handle any errors
                                                Card Item = new Card(child.getKey(), child.child("Name").getValue().toString(), "default", child.child("Biography").getValue().toString(), 1);
                                                rowItems.add(Item);
                                                arrayAdapter.notifyItemInserted(rowItems.size()-1);
                                            }
                                        });
                                    }
                                }
                            });



                        }

                    }



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void sendOnChannel1(View v)
    {
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_baseline_attach_email_24)
                .setContentTitle("You have matched with someone!")
                .setContentText("Find out who!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_EVENT)
                .build();

        nManager.notify(1, notification);
    }


}